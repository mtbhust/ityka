package com.example.login.dto;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class RegistrationInfoDTO {
    private final String firstName;
    private final String lastName;
    private final String email;
    private final String password;
}
